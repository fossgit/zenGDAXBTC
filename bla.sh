#/bin/bash

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install -y git build-essential mongodb nodejs

git clone https://github.com/deviavir/zenbot.git
cd zenbot

npm install

./zenbot.sh --help

sh update.sh

cp conf-sample.js conf.js

echo price?
